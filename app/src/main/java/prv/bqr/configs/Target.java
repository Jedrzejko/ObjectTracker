package prv.bqr.configs;

import java.net.FileNameMap;

/**
 * Created by m_jedrzejko on 21.10.15.
 */
public class Target {
    public static final String GUN = "gun";
    public static final String SCHEMA = "schema";
    public static final String NOTE2 = "note2";
    public static final String MARKERS = "markers";
    public static final String SWITCHER = "switcher";
    public static final String PRIVATE = "szeregowy";
    public static final String BOX = "box";
    public static final String CAR = "car";
}
